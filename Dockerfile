#FROM openjdk:8u111-jdk-alpine
#VOLUME /tmp
#ADD /target/*.jar app.jar
#ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]

#
# Build stage
#
FROM maven:3.6.0-jdk-11-slim AS build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package

#
# Package stage
#
FROM openjdk:11-jre-slim
COPY --from=build /home/app/target/*.jar /usr/local/lib/mainapp.jar
EXPOSE 8090
ENTRYPOINT ["java","-jar","/usr/local/lib/mainapp.jar"]
