package com.example.actuatorsample;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
 
    @RequestMapping(value = "/hello", method = RequestMethod.GET)
	public String index() {
		return "Greetings from Spring Boot!";
	}

    @RequestMapping(value = "/greeting", method = RequestMethod.GET)
	public String greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
		return "Greetings " + name ;
	}
	
	
}
	